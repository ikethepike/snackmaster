<?php

namespace Snackmaster;

use Illuminate\Database\Eloquent\Model;

class Cuisine extends Model
{
    // Fillable
    protected $fillable = ['snacks', 'adjective', 'type', 'week_id'];

    protected $adjectives = ['salty', 'sweet', 'nutty', 'baked', 'exotic', 'crunchy', 'sloppy', 'classy', 'spicy', 'asian', 'european', 'american', 'savory', 'south american', 'terrifying', 'british', 'german', 'japanese', 'candy', 'french', 'italian', 'seedy', 'questionable', 'adventurous', 'traditional', 'green', 'blue', 'red', 'chic', 'cosmopolitan', 'new', 'mindboggling', 'posh', 'regional', 'swedish', 'space-age', 'life-changing', 'revelatory', 'pirate-y', 'finnish', 'fingerlicking', 'homemade'];

    protected $types = ['italian', 'pizza', 'asian', 'swedish', 'mexican', 'south american', 'vietnamese', 'indian', 'japanese', 'continental', "Xan's famous spaghetti", 'chinese', 'thai', 'greek', 'georgian', 'deli', 'burgers', 'middle-eastern', "Saga's Japanese I guess", 'spanish', 'american', 'hipster', 'fusion', 'fast-food', 'rawfood', 'salad', 'danish', 'scandinavian', 'Mediterranean', 'egyptian', 'african', 'caribbean', 'russian', "Daniel's mystery slop", 'soup', 'waffles', 'canadian', 'wraps', 'cake', 'pie', 'pasta', 'baked'];

    /**
     * Returns a type of snack.
     *
     * @return string
     */
    public function snack()
    {
        return $this->adjectives[mt_rand(0, count($this->adjectives) - 1)];
    }

    /**
     * Return an adjective.
     */
    public function adjective()
    {
        return $this->snack();
    }

    /**
     * Return a type of cuisine.
     */
    public function type()
    {
        return $this->types[mt_rand(0, count($this->types) - 1)];
    }
}
