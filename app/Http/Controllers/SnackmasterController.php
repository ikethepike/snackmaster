<?php

namespace Snackmaster\Http\Controllers;

use Snackmaster\Week;
use Snackmaster\Master;
use Snackmaster\Cuisine;
use Snackmaster\Scrivner;
use Illuminate\Support\Carbon;

class SnackmasterController extends Controller
{
    public function assign()
    {
        // Sanity check
        $current = $this->current();

        if ($current) {
            return $current;
        }

        // Find the last snackmaster and make sure we aren't double setting
        $lastWeek = Week::orderBy('id', 'desc')->first();

        // Pick a random master
        $masters = Master::inRandomOrder()->where('eligible', true)->get();

        if (!$lastWeek) {
            $previous = $masters[0];
        } else {
            $lastWeek->load('master');
            $previous = $lastWeek->master;
        }

        $snackmaster = $masters[0]->id !== $previous->id ? $masters[0]->id : $masters[1]->id;

        // Attach to week
        $week = Week::create([
            'number'    => (int) date('W'),
            'master_id' => $snackmaster,
        ]);

        $cuisine = new Cuisine();

        $week->cuisine()->create([
            'type'      => $cuisine->type(),
            'snacks'    => $cuisine->snack(),
            'adjective' => $cuisine->adjective(),
        ]);

        $scrivner = new Scrivner();

        $week->scrivner()->create([
            'epithet'   => $scrivner->epithet(),
            'master_id' => $snackmaster === $previous->id ? $masters[2]->id : $masters[1]->id,
        ]);

        return $week->load(['master', 'cuisine', 'scrivner']);
    }

    public function current()
    {
        $date = Carbon::now();
        $date = $date->subWeek();

        return Week::orderBy('id', 'desc')->where('created_at', '>', $date)->first();
    }

    public function history()
    {
        return Week::all();
    }

    public function stats()
    {
        $weeks   = Week::all();
        $masters = Master::all();

        $stats   = collect([
            'masters' => collect([]),
            'weeks'   => $weeks->count(),
        ]);

        foreach ($masters as $master) {
            $absolute = $weeks->where('master_id', $master->id)->count();
            $decimal  = $absolute / $weeks->count();

            $stats['masters']->push([
                'master'    => $master,
                'incidence' => [
                    'absolute'   => $absolute,
                    'decimal'    => $decimal,
                    'percentage' => round($decimal * 100) . '%',
                ],
            ]);
        }

        return $stats;
    }
}
