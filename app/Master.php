<?php

namespace Snackmaster;

use Illuminate\Database\Eloquent\Model;

class Master extends Model
{
    protected $fillable = ["name"];
    
    public function weeks()
    {
        return $this->hasMany(Week::class);
    }
}
