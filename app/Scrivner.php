<?php

namespace Snackmaster;

use Illuminate\Database\Eloquent\Model;

class Scrivner extends Model
{
    // fillable
    protected $fillable = ['master_id', 'epithet', 'week_id'];

    // Default relations
    protected $with = ['master'];

    protected $epithets = ['fair', 'strong', 'brave', 'booger-eater', 'skullcrusher', 'spellcaster', 'paragon', 'sinner', 'lascivious', 'wise', 'seventeen nippled', 'rock', 'stump', 'bucket', 'log', 'goat', 'rat', 'flogger',  'cyst-drinker', 'unknown', 'intelligent', 'secretive', 'speaker of tongues', 'bonebreaker', 'witchdoctor', 'clump', 'slobberer', 'disinterested', 'hairy footed', 'fanged', 'strict', 'mad', 'bold', 'gouty', 'idiot', 'book-lover', 'yoga-mom', 'fart',  'bomb', 'fickle', 'squeamish', 'bitten', 'leprous', 'trembler', 'fiddler', 'entertainer', 'impotent', 'cabbage', 'boneless', 'lame', 'slit-nosed', 'luxurious', 'debonaire', 'stammerer', 'sausage-maker', 'tit', 'short', 'tall', 'beautiful', 'scalper', 'caped', 'hoarse', 'cross-eyed', 'hairy', 'monstruous', 'silent', 'bastard', 'hermit', 'nipple', 'toe', 'arm', 'foot', 'nose', 'mouth', 'ass', 'horse', 'squisher', 'seeper', 'cysty', 'brigand', 'syphilitic', 'bard', 'barbarian', 'hunk', 'weak', 'slackjawed', 'hung', 'moron', 'drooler', 'flappy-eared', 'lover', 'mace', 'hammer', 'axe', 'runner', 'incompetent', 'brave', 'mechanic', 'seer', 'flagon', 'scrotum', 'peeper', 'slurper', 'one-eyed', 'pirate', 'ruler of dragons', 'plugger of butts', 'sleepy', 'dozy', 'droopy', 'undescended', 'unscented', 'dancer', 'spicy', 'zesty', 'tan, taught and tasty', 'nasty', 'hungry', 'endowed', 'worm', 'slug', 'warbler', 'chanter', 'mechanic of cars', 'strider', 'easy-rider', 'vampire', 'werewolf', 'husky', 'bear', 'festering', 'gallant', 'studmuffin', 'bootylicious'];

    public function epithet()
    {
        return $this->epithets[mt_rand(0, count($this->epithets) - 1)];
    }

    public function master()
    {
        return $this->belongsTo(Master::class);
    }
}
