<?php

namespace Snackmaster;

use Illuminate\Database\Eloquent\Model;

class Week extends Model
{
    protected $fillable = ['master_id', 'number', "scrivner_id", "week"];

    protected $with = ['master', 'cuisine', "scrivner"];

    public function master()
    {
        return $this->belongsTo(Master::class);
    }

    public function cuisine()
    {
        return $this->hasOne(Cuisine::class);
    }

    public function scrivner()
    {
        return $this->hasOne(Scrivner::class);
    }
    
}
