<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimestampsToCuisinesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('cuisines', function (Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('cuisines', function (Blueprint $table) {
            //
        });
    }
}
