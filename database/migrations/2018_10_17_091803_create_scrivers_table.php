<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScriversTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('scrivners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('master_id')->unsigned();
            $table->string('epithet');

            $table->foreign('master_id')->references('id')->on('masters')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('scrivers');
    }
}
