<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEligibleToMasters extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('masters', function (Blueprint $table) {
            $table->boolean('eligible')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('masters', function (Blueprint $table) {
            //
        });
    }
}
