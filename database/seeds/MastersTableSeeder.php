<?php


use Snackmaster\Master;
use Illuminate\Database\Seeder;

class MastersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $people = ['Daniel', 'Kalle', 'Xan', 'Isaac'];

        foreach ($people as $person) {
            Master::create([
                'name' => $person,
            ]);
        }
    }
}
