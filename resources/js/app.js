// Boot up the project
import './init'
import store from './store'

// Start up vue
import Vue from 'vue'

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Main app wrapper
import App from './components/App.vue'

const app = new Vue({
    store,
    components: { App },
    render: h => h(App),
}).$mount('#snackmaster-app')
