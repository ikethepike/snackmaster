/**
 * Starts playing dramatic music
 * @param { state, commit, rootState } param
 * @param {*}
 */
export const drama = ({ commit }) => {
    return new Promise((resolve, reject) => {
        const audio = new Audio()

        audio.src = `${Snackmaster.url}/audio/drama.mp3`

        const playable = () => {
            audio.play()
            commit('playing', true)
            return resolve(true)
        }

        audio.addEventListener('canplaythrough', playable)
        audio.addEventListener('canplay', playable)

        audio.load()
    })
}

/**
 * Get snackmaster
 * @param { state, commit, rootState } param
 * @param {*}
 */
export const getSnackmaster = ({ commit }) => {
    console.log('getting!')
    return new Promise((resolve, reject) => {
        axios
            .get('snackmaster/current')
            .then(response => {
                if (response.data) {
                    commit('snackmaster', response.data)
                }

                return resolve(response.data)
            })
            .catch(error => {
                return reject(error)
            })
    })
}

/**
 * Assigns a new snackmaster
 * @param { state, commit, rootState } param
 * @param {*}
 */
export const newSnackmaster = ({ commit }) => {
    axios
        .post('snackmaster/assign')
        .then(response => {
            commit('snackmaster', response.data)
        })
        .catch(error => {
            console.log('ruh roh')
            console.log(error)
        })
}

/**
 * Retrieves the stats
 * @param { state, commit, rootState } param
 * @param {*}
 */
export const getStats = ({ commit }) => {
    return new Promise((resolve, reject) => {
        axios
            .get('snackmaster/stats')
            .then(response => {
                commit('stats', response.data)
                return resolve(response.data)
            })
            .catch(error => {
                return reject({
                    type: 'error',
                    text: 'Error fetching stats',
                    status: error.response.status,
                })
            })
    })
}
