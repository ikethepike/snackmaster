/**
 * Returns the current snackmaster
 * @param {state, getters, rootState, rootGetters}
 * @return state.snackmaster
 */
export const snackmaster = state => {
    return state.snackmaster ? state.snackmaster.master : null
}

/**
 * week
 * @param {state, getters, rootState, rootGetters}
 * @return state.week
 */
export const week = state => {
    return state.snackmaster ? state.snackmaster.number : null
}

/**
 * cuisine
 * @param {state, getters, rootState, rootGetters}
 * @return state.cuisine
 */
export const cuisine = state => {
    return state.snackmaster ? state.snackmaster.cuisine : null
}

/**
 * stats
 * @param {state, getters, rootState, rootGetters}
 * @return state.stats
 */
export const stats = state => {
    return state.stats
}

/**
 * scrivner
 * @param {state, getters, rootState, rootGetters}
 * @return state.scrivner
 */
export const scrivner = state => {
    return state.snackmaster ? state.snackmaster.scrivner : null
}

