/**
 * playing mutation
 * @param playing
 */
export const playing = (state, playing) => {
    return (state.playing = playing)
}

/**
 * snackmaster mutation
 * @param snackmaster
 */
export const snackmaster = (state, snackmaster) => {
    return (state.snackmaster = snackmaster)
}

/**
 * stats mutation
 * @param stats
 */
export const stats = (state, stats) => {
    return (state.stats = stats)
}
