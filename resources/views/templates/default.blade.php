<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title> @yield('title', env('APP_NAME')) </title>

	<link rel="stylesheet" href="{{  mix('/css/app.css') }}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	
	{{-- favicons --}}
	@include("templates.parts.favicons") 
	@yield("head")

	<script>
		window.Snackmaster = {!! json_encode([
			'url'       	=> url('/'),
		]) !!}
	</script>
</head>

<body class='@yield("page")'>

	<div id="snackmaster-app" ref="main">
		@yield('content')
	</div>

	{{-- Main scripts, vendor dependencies and manifest --}} 
	@yield('scripts')

	<script type="text/javascript" src="{{ asset('js/app.js') }}" defer></script>
	{{-- <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script> --}}
</body>
</html>