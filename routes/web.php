<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('landing');
});

Route::group(['prefix' => 'snackmaster'], function () {
    // Set a new snackmaster
    Route::post('assign', 'SnackmasterController@assign');

    // Get the current snackmaster
    Route::get('current', 'SnackmasterController@current');

    // Get a listing of each week
    Route::get('history', 'SnackmasterController@history');

    // Return stats
    Route::get('stats', 'SnackmasterController@stats');
});



