const mix = require('laravel-mix')

mix.browserSync('snackmaster.abc')

mix.js('resources/js/app.js', 'public/js').sass(
    'resources/sass/app.scss',
    'public/css'
)
